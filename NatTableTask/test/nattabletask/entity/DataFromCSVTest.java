package nattabletask.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import nattabletask.utils.CSVReadersList;
import nattabletask.utils.CSVReadersList.CSVReader;

public class DataFromCSVTest {

	@Test
	public void removeFromToWorkFine() {
		try {
			String path = "C:\\Users\\Illia\\git\\nattabletask\\NatTableTask\\Folder_WITH_CSV\\file2.csv";
			DataFromCSV dataFromCSV = new DataFromCSV(path);
			CSVReader csvReader = CSVReadersList.getCsvReader(path);
			csvReader.setNumberOfRow(2);
			dataFromCSV.addAllHeaderElement(csvReader.readHeader());
			dataFromCSV.addAllElement(csvReader.readNext());
			dataFromCSV.addAllElement(csvReader.readNext());
			dataFromCSV.addAllElement(csvReader.readNext());

			dataFromCSV.removeFromTo(0, csvReader.getNumberOfRow() * 2);

			String expected1 = "[Name, Group, SWTDone]\r\n" + "============\r\n" + "[Petrov, 2, false]\r\n"
					+ "[Dedun, 100, true]";

			assertEquals(expected1, dataFromCSV.toString());

		} catch (FileNotFoundException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void equalsWorkFine() {
		DataFromCSV data1 = new DataFromCSV("path");
		DataFromCSV data2 = new DataFromCSV("path");
		DataFromCSV data3 = new DataFromCSV("other path");

		assertTrue(data1.equals(data2));
		assertTrue(data2.equals(data1));

		assertFalse(data1.equals(data3));
		assertFalse(data2.equals(data3));

		assertTrue(data3.equals(data3));

		assertFalse(data2.equals(null));
	}

	@Test
	public void testHashCodeWorkFine() {
		DataFromCSV data1 = new DataFromCSV("path");
		DataFromCSV data2 = new DataFromCSV("path");
		DataFromCSV data3 = new DataFromCSV("other path");

		assertEquals(data1.hashCode(), data2.hashCode());
		assertEquals(data2.hashCode(), data1.hashCode());

		assertNotEquals(data1.hashCode(), data3.hashCode());
		assertNotEquals(data2.hashCode(), data3.hashCode());

		assertEquals(data3.hashCode(), data3.hashCode());
	}

}
