package nattabletask.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import nattabletask.entity.DataFromCSV;
import nattabletask.utils.CSVReadersList.CSVReader;

public class CSVReadersListTest {

	@Test
	public void CSVReadersListWorkFine() {
		try {
			String path = "file2.csv";
			DataFromCSV dataFromCSV = new DataFromCSV(path);
			CSVReader csvReader = CSVReadersList.getCsvReader(path);
			csvReader.setNumberOfRow(2);
			dataFromCSV.addAllHeaderElement(csvReader.readHeader());
			dataFromCSV.addAllElement(csvReader.readNext());
			dataFromCSV.addAllElement(csvReader.readNext());
			dataFromCSV.addAllElement(csvReader.readNext());
			dataFromCSV.addAllElement(csvReader.readNext());
			dataFromCSV.addAllElement(csvReader.readNext());
			dataFromCSV.addAllElement(csvReader.readNext());

			String expected1 = "[Name, Group, SWTDone]\r\n" + "============\r\n" + "[Nykolenko, 1, true]\r\n"
					+ "[Pir, 155, true]\r\n" + "[Sorokin, 5, true]\r\n" + "[as, 2, true]\r\n" + "[Petrov, 2, false]\r\n"
					+ "[Dedun, 100, true]\r\n" + "[Putin, 0, false]\r\n" + "[Dedun, 15, true]\r\n"
					+ "[Lysenko, 4, false]\r\n" + "[10yan, 2, false]\r\n" + "[Nykolenko, 1, true]\r\n"
					+ "[Pir, 155, true]";

			assertEquals(expected1, dataFromCSV.toString());

			dataFromCSV.addAllElement(0, csvReader.readPrevious());
			dataFromCSV.addAllElement(0, csvReader.readPrevious());
			String expected2 = "[Name, Group, SWTDone]\r\n" + "============\r\n" + "[Nykolenko, 1, true]\r\n"
					+ "[Pir, 155, true]\r\n" + "[Sorokin, 5, true]\r\n" + "[as, 2, true]\r\n"
					+ "[Nykolenko, 1, true]\r\n" + "[Pir, 155, true]\r\n" + "[Sorokin, 5, true]\r\n"
					+ "[as, 2, true]\r\n" + "[Petrov, 2, false]\r\n" + "[Dedun, 100, true]\r\n"
					+ "[Putin, 0, false]\r\n" + "[Dedun, 15, true]\r\n" + "[Lysenko, 4, false]\r\n"
					+ "[10yan, 2, false]\r\n" + "[Nykolenko, 1, true]\r\n" + "[Pir, 155, true]";
			assertEquals(expected2, dataFromCSV.toString());
		} catch (FileNotFoundException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}

}