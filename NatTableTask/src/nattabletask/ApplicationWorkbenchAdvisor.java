package nattabletask;

import java.io.File;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import nattabletask.editor.DataEdior;
import nattabletask.editor.DataEditorInput;
import nattabletask.entity.DataFromCSV;
import nattabletask.utils.CSVReadersList;
import nattabletask.view.NavigationView;

public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

	private static final String PERSPECTIVE_ID = "NatTableTask.perspective"; //$NON-NLS-1$
	private CTabFolder cTabFolder;

	@Override
	public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		return new ApplicationWorkbenchWindowAdvisor(configurer);
	}

	@Override
	public String getInitialWindowPerspectiveId() {
		return PERSPECTIVE_ID;
	}

	@Override
	public void postStartup() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		findCTabFolder(window.getShell().getChildren());
		dnd();
		window.getActivePage().addPartListener(new CustomPartListener());
	}

	private void findCTabFolder(Control[] controls) {
		for (Control control : controls) {
			if (control instanceof CTabFolder) {
				cTabFolder = (CTabFolder) control;
				break;
			} else if (control instanceof Composite) {
				findCTabFolder(((Composite) control).getChildren());
			}
		}
	}

	private void dnd() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		NavigationView navigationView = (NavigationView) window.getActivePage().findView(NavigationView.ID);
		DragSource ds = new DragSource(navigationView.getViewer().getTree(), DND.DROP_MOVE);
		ds.setTransfer(new Transfer[] { TextTransfer.getInstance() });
		ds.addDragListener(new DragSourceAdapter() {
			@Override
			public void dragSetData(DragSourceEvent event) {
				IStructuredSelection selection = navigationView.getViewer().getStructuredSelection();
				File file = (File) selection.getFirstElement();
				event.data = file.getAbsolutePath();
			}
		});

		DropTarget dt = new DropTarget(cTabFolder, DND.DROP_MOVE);
		dt.setTransfer(new Transfer[] { TextTransfer.getInstance() });
		dt.addDropListener(new DropTargetAdapter() {
			@Override
			public void drop(DropTargetEvent event) {
				navigationView.openFile(event.data.toString(), window);
			}
		});
	}

	class CustomPartListener implements IPartListener {

		@Override
		public void partActivated(IWorkbenchPart part) {
			// empty
		}

		@Override
		public void partBroughtToTop(IWorkbenchPart part) {
			// empty
		}

		@Override
		public void partClosed(IWorkbenchPart part) {
			if (part instanceof DataEdior) {
				DataEdior dataEdior = (DataEdior) part;
				DataFromCSV dataFromCSV = ((DataEditorInput) dataEdior.getEditorInput()).getDataFromCSV();
//					InnerCSVReader csvReader = CSVReader.getCsvReader(dataFromCSV.getFileAbsolutePath());
				CSVReadersList.removeReader(dataFromCSV.getFileAbsolutePath());

			}
		}

		@Override
		public void partDeactivated(IWorkbenchPart part) {
			// empty
		}

		@Override
		public void partOpened(IWorkbenchPart part) {
			// empty
		}

	}
}
