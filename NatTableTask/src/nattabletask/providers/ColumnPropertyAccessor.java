package nattabletask.providers;

import java.util.List;

import org.eclipse.nebula.widgets.nattable.data.IColumnPropertyAccessor;

import nattabletask.entity.DataFromCSV;

public class ColumnPropertyAccessor implements IColumnPropertyAccessor<List<String>> {

	private DataFromCSV dataFromCSV;

	public ColumnPropertyAccessor(DataFromCSV dataFromCSV) {
		this.dataFromCSV = dataFromCSV;
	}

	@Override
	public Object getDataValue(List<String> rowObject, int columnIndex) {
		if (columnIndex >= rowObject.size()) {
			return "";
		}
		return rowObject.get(columnIndex);
	}

	@Override
	public void setDataValue(List<String> rowObject, int columnIndex, Object newValue) {
		throw new UnsupportedOperationException();

	}

	@Override
	public int getColumnCount() {
		return dataFromCSV.numberOfColumn();
	}

	@Override
	public String getColumnProperty(int columnIndex) {
		return dataFromCSV.getHeaderList().get(columnIndex);
	}

	@Override
	public int getColumnIndex(String propertyName) {
		return dataFromCSV.getHeaderList().indexOf(propertyName);
	}
}
