package nattabletask.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import nattabletask.Activator;
import nattabletask.editor.DataEdior;
import nattabletask.editor.DataEditorInput;
import nattabletask.entity.DataFromCSV;
import nattabletask.utils.CSVReadersList;
import nattabletask.utils.CSVReadersList.CSVReader;

public class NavigationView extends ViewPart {
	public static final String ID = "nattabletask.view.navigationView";
	private static final Logger logger = Logger.getLogger(NavigationView.class.getName());
	private TreeViewer viewer;
	private String path;

	@Override
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewLabelProvider()));

		getSite().setSelectionProvider(viewer);

		viewer.addDoubleClickListener(d -> {
			Object selectObj = ((IStructuredSelection) d.getSelection()).getFirstElement();
			File file = (File) selectObj;
			openFile(file.getAbsolutePath(), PlatformUI.getWorkbench().getActiveWorkbenchWindow());
		});
		
	}

	class ViewContentProvider implements ITreeContentProvider {
		Pattern pattern = Pattern.compile(".csv");

		@Override
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
			// empty
		}

		@Override
		public void dispose() {
			// empty
		}

		@Override
		public Object[] getElements(Object inputElement) {
			File[] arr = (File[]) inputElement;
			File[] newArr = new File[1];
			String fileName = new File(path).getName();
			for (File file : arr) {
				if (file.getName().equals(fileName)) {
					newArr[0] = file;
				}
			}
			return newArr;
		}

		@Override
		public Object[] getChildren(Object parentElement) {
			File file = (File) parentElement;
			Stream<File> streamDir = Arrays.stream(file.listFiles());
			Stream<File> streamFile = Arrays.stream(file.listFiles());

			return Stream.concat(streamDir.filter(str -> str.isDirectory()),
					streamFile.filter(str -> pattern.matcher(str.getName()).find())).toArray(File[]::new);
		}

		@Override
		public Object getParent(Object element) {
			File file = (File) element;
			return file.getParentFile();
		}

		@Override
		public boolean hasChildren(Object element) {
			File file = (File) element;
			return file.isDirectory();
		}

	}

	class ViewLabelProvider extends LabelProvider implements IStyledLabelProvider {

		private ImageDescriptor directoryImage;
		private ImageDescriptor fileImage;
		private ResourceManager resourceManager;

		public ViewLabelProvider() {
			this.directoryImage = Activator.getImageDescriptor("/icons/folder.png");
			this.fileImage = Activator.getImageDescriptor("/icons/page_white.png");
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof File) {
				File file = (File) element;
				StyledString styledString = new StyledString(getFileName(file).replaceFirst("[.][^.]+$", ""));
				String[] files = file.list();
				if (files != null) {
					styledString.append(" ( " + files.length + " ) ", StyledString.COUNTER_STYLER);
				}
				return styledString;

			}
			return null;
		}

		@Override
		public Image getImage(Object element) {
			if (element instanceof File) {
				if (((File) element).isDirectory()) {
					return getResourceManager().createImage(directoryImage);
				} else {
					return getResourceManager().createImage(fileImage);
				}
			}

			return super.getImage(element);
		}

		@Override
		public void dispose() {
			if (resourceManager != null) {
				resourceManager.dispose();
				resourceManager = null;
			}
		}

		protected ResourceManager getResourceManager() {
			if (resourceManager == null) {
				resourceManager = new LocalResourceManager(JFaceResources.getResources());
			}
			return resourceManager;
		}

		private String getFileName(File file) {
			String name = file.getName();
			return name.isEmpty() ? file.getPath() : name;
		}
	}

	public void setFocus() {
		viewer.getControl().setFocus();
	}

	public void refreshTree() {
		Object[] elements = viewer.getExpandedElements();
		File file = new File(path);
		viewer.setInput(new File(file.getParent()).listFiles());
		viewer.setExpandedElements(elements);
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public TreeViewer getViewer() {
		return viewer;
	}

	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	public void createErrorDialog(Throwable t, IWorkbenchWindow window) {

		List<Status> childStatuses = new ArrayList<>();
		StackTraceElement[] stackTraces = Thread.currentThread().getStackTrace();

		for (StackTraceElement stackTrace : stackTraces) {
			Status status = new Status(IStatus.ERROR, "JFaceTask", stackTrace.toString());
			childStatuses.add(status);
		}
		MultiStatus status = new MultiStatus("JFaceTask", IStatus.ERROR, childStatuses.toArray(new Status[] {}),
				t.toString(), t);
		ErrorDialog.openError(window.getShell(), "Error", "This is an error", status);

	}

	public void openFile(String filePath, IWorkbenchWindow window) {
		IWorkbenchPage page = window.getActivePage();
		File fileToRead = new File(filePath);
		if (!fileToRead.isDirectory()) {
			DataFromCSV dataFromCSV = new DataFromCSV(fileToRead.getAbsolutePath());
			try {
				CSVReader csvReader = CSVReadersList.getCsvReader(dataFromCSV.getFileAbsolutePath());
				dataFromCSV.addAllHeaderElement(csvReader.readHeader());
				dataFromCSV.addAllElement(csvReader.readNext());
				dataFromCSV.addAllElement(csvReader.readNext());
			} catch (FileNotFoundException e1) {
				logger.log(Level.INFO, e1.getMessage());
				createErrorDialog(e1, window);
			} catch (IOException e1) {
				logger.log(Level.INFO, e1.getMessage());
				createErrorDialog(e1, window);
			}

			DataEditorInput input = new DataEditorInput(dataFromCSV);
			boolean found = false;

			if (dataFromCSV.getFileName() != null) {
				IEditorReference[] eRefs = page.getEditorReferences();
				for (IEditorReference ref : eRefs) {
					IEditorPart editor = ref.getEditor(false);
					if (editor instanceof DataEdior) {
						DataEdior dataEditor = (DataEdior) ref.getEditor(true);
						DataFromCSV dataFromInput = ((DataEditorInput) dataEditor.getEditorInput()).getDataFromCSV();
						if (dataFromCSV.getFileAbsolutePath().equals(dataFromInput.getFileAbsolutePath())) {
							found = true;
							MessageDialog.openInformation(window.getShell(), "Info", "This file is already open.");
							break;
						}
					}
				}
			}
			if (!found) {
				try {
					page.openEditor(input, DataEdior.ID);
				} catch (PartInitException e) {
					logger.log(Level.INFO, e.getMessage());
				}
			}
		} else {
			MessageDialog.openInformation(window.getShell(), "Info", "You can not open the folder!\nTry open file.");
		}
	}

}