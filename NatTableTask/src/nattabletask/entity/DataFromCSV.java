package nattabletask.entity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DataFromCSV {
	private List<List<String>> list;
	private List<String> headerList;
	private String pathToCsv;

	public DataFromCSV(String pathToCsv) {
		this.list = new ArrayList<>();
		this.headerList = new ArrayList<>();
		this.pathToCsv = pathToCsv;
	}

	public List<List<String>> getList() {
		return new ArrayList<>(list);
	}

	public void addElement(List<String> element) {
		list.add(element);
	}

	public void addAllElement(List<? extends List<String>> newList) {
		list.addAll(newList);
	}

	public void addAllElement(int index, List<? extends List<String>> newList) {
		list.addAll(index, newList);
	}

	public void removeFromTo(int fromIndex, int toIndex) {
		for (int i = fromIndex; i < toIndex; i++) {
			list.remove(fromIndex);
		}
	}

	public int numberOfRow() {
		return list.size();
	}

	public int numberOfColumn() {
		return headerList.size();
	}

	public List<String> getHeaderList() {
		return new ArrayList<>(headerList);
	}

	public void addAllHeaderElement(List<? extends String> newList) {
		headerList.addAll(newList);
	}

	public String[] headerListToArray() {
		return headerList.toArray(new String[0]);
	}

	public Map<String, String> headerListToMap() {
		Map<String, String> map = new HashMap<>();
		for (String s : headerList) {
			map.put(s, s.toUpperCase());
		}
		return map;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(headerList).append(System.lineSeparator());
		stringBuilder.append("============").append(System.lineSeparator());
		for (List<String> l : list) {
			stringBuilder.append(l).append(System.lineSeparator());
		}
		return stringBuilder.toString().trim();
	}

	public String getFileAbsolutePath() {
		return new File(pathToCsv).getAbsolutePath();
	}

	public String getFileName() {
		return new File(pathToCsv).getName().replaceFirst("[.][^.]+$", "");
	}

	@Override
	public int hashCode() {
		return Objects.hash(pathToCsv);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof DataFromCSV)) {
			return false;
		}
		DataFromCSV other = (DataFromCSV) obj;
		return this.pathToCsv.equals(other.pathToCsv);
	}
}
