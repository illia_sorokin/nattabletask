package nattabletask.editor;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import nattabletask.entity.DataFromCSV;

public class DataEditorInput implements IEditorInput {

	private DataFromCSV dataFromCSV;

	public DataEditorInput(DataFromCSV dataFromCSV) {
		this.dataFromCSV = dataFromCSV;
	}

	public DataFromCSV getDataFromCSV() {
		return dataFromCSV;
	}

	@Override
	public <T> T getAdapter(Class<T> adapter) {
		return null;
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	@Override
	public String getName() {
		return dataFromCSV != null ? dataFromCSV.getFileName() : "New";
	}

	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	@Override
	public String getToolTipText() {
		return "Table Tab";
	}
}
