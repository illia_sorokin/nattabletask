package nattabletask.editor;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.data.IColumnPropertyAccessor;
import org.eclipse.nebula.widgets.nattable.data.IDataProvider;
import org.eclipse.nebula.widgets.nattable.data.ListDataProvider;
import org.eclipse.nebula.widgets.nattable.grid.GridRegion;
import org.eclipse.nebula.widgets.nattable.grid.data.DefaultColumnHeaderDataProvider;
import org.eclipse.nebula.widgets.nattable.grid.layer.ColumnHeaderLayer;
import org.eclipse.nebula.widgets.nattable.layer.CompositeLayer;
import org.eclipse.nebula.widgets.nattable.layer.DataLayer;
import org.eclipse.nebula.widgets.nattable.layer.ILayer;
import org.eclipse.nebula.widgets.nattable.viewport.ViewportLayer;
import org.eclipse.nebula.widgets.nattable.viewport.command.ShowRowInViewportCommand;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import nattabletask.entity.DataFromCSV;
import nattabletask.providers.ColumnPropertyAccessor;
import nattabletask.utils.CSVReadersList;
import nattabletask.utils.CSVReadersList.CSVReader;

public class DataEdior extends AbstractBaseEditor {

	public static final String ID = "nattabletask.editor.dataEditor";
	private NatTable natTable;
	private static final Logger logger = Logger.getLogger(DataEdior.class.getName());

	@Override
	public void createPartControl2(Composite parent) {

		parent.setLayout(new GridLayout());

		DataEditorInput editorInput = (DataEditorInput) getEditorInput();

		DataFromCSV dataFromCSV = editorInput.getDataFromCSV();
		CSVReader csvReader = null;
		try {
			csvReader = CSVReadersList.getCsvReader(dataFromCSV.getFileAbsolutePath());
		} catch (FileNotFoundException e) {
			logger.log(Level.INFO, e.getMessage());
		}
		setPartName(dataFromCSV.getFileName());

		IColumnPropertyAccessor<List<String>> columnPropertyAccessor = new ColumnPropertyAccessor(dataFromCSV);
		ListDataProvider<List<String>> bodyDataProvider = new ListDataProvider<>(dataFromCSV.getList(),
				columnPropertyAccessor);

		DataLayer bodyDataLayer = new DataLayer(bodyDataProvider);

		bodyDataLayer.setColumnPercentageSizing(true);

		ViewportLayer viewportLayer = new ViewportLayer(bodyDataLayer);
		viewportLayer.setRegionName(GridRegion.BODY);

		IDataProvider headerDataProvider = new DefaultColumnHeaderDataProvider(dataFromCSV.headerListToArray(),
				dataFromCSV.headerListToMap());
		DataLayer headerDataLayer = new DataLayer(headerDataProvider);
		ILayer columnHeaderLayer = new ColumnHeaderLayer(headerDataLayer, viewportLayer);

		CompositeLayer compositeLayer = new CompositeLayer(1, 2);
		compositeLayer.setChildLayer(GridRegion.COLUMN_HEADER, columnHeaderLayer, 0, 0);
		compositeLayer.setChildLayer(GridRegion.BODY, viewportLayer, 0, 1);

		natTable = new NatTable(parent, compositeLayer);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(natTable);

		natTable.getVerticalBar()
				.addSelectionListener(new CustomSelectionListener(dataFromCSV, bodyDataProvider, csvReader));

	}

	@Override
	public void setFocus() {
		natTable.setFocus();
	}

	class CustomPaintListener implements PaintListener {
		private int index;

		public CustomPaintListener(int index) {
			this.index = index;
		}

		@Override
		public void paintControl(PaintEvent e) {
			natTable.doCommand(new ShowRowInViewportCommand(index));
			natTable.removePaintListener(this);
		}
	}

	class CustomSelectionListener extends SelectionAdapter {

		private DataFromCSV dataFromCSV;
		private ListDataProvider<List<String>> bodyDataProvider;
		private CSVReader csvReader;
		private int rowCount;
		private int groupCount;
		private int startFileGroupCount;
		private int lengthOfNextNewList;
		private boolean isEndOfFileReached;
		private boolean isStartOfFilReached;

		public CustomSelectionListener(DataFromCSV dataFromCSV, ListDataProvider<List<String>> bodyDataProvider,
				CSVReader csvReader) {
			this.dataFromCSV = dataFromCSV;
			this.bodyDataProvider = bodyDataProvider;
			this.csvReader = csvReader;
			this.rowCount = 0;
			this.startFileGroupCount = 0;
			this.groupCount = 0;
			this.lengthOfNextNewList = csvReader.getNumberOfRow();
			this.isStartOfFilReached = false;
			this.isEndOfFileReached = false;

		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			final int numberOfRowsInOnePortion = csvReader.getNumberOfRow();
			final int numberOfRowsInAllTable = numberOfRowsInOnePortion * 4;
			final int rowHeight = natTable.getRowHeightByPosition(0);
			final int numberOfRowsOnScreen = natTable.getClientArea().height / rowHeight - 1;
			final int onehundredRowsHeight = rowHeight * numberOfRowsInOnePortion;

			try {
				if (rowCount > natTable.getVerticalBar().getSelection()) {

					rowCount = natTable.getVerticalBar().getSelection();

					if (groupCount >= 1 && rowCount - onehundredRowsHeight * (groupCount - 1) <= onehundredRowsHeight) {
						if (!isEndOfFileReached) {
							int length = numberOfRowsInOnePortion * 3;
							List<List<String>> newList = csvReader.readPrevious();

							if (newList.isEmpty()) {
								isStartOfFilReached = true;
							}

							dataFromCSV.addAllElement(0, newList);
							bodyDataProvider.getList().addAll(0, newList);

							if (bodyDataProvider.getList().size() > numberOfRowsInAllTable) {
								if (lengthOfNextNewList != numberOfRowsInOnePortion) {
									removeElementFromTo(bodyDataProvider.getList(), numberOfRowsInOnePortion,
											numberOfRowsInOnePortion * 2 - lengthOfNextNewList);
									dataFromCSV.removeFromTo(numberOfRowsInOnePortion,
											numberOfRowsInOnePortion * 2 - lengthOfNextNewList);
									length = lengthOfNextNewList + numberOfRowsInOnePortion * 2;
									lengthOfNextNewList = numberOfRowsInOnePortion;
								}

								removeElementFromTo(bodyDataProvider.getList(), numberOfRowsInAllTable,
										bodyDataProvider.getList().size());
								dataFromCSV.removeFromTo(numberOfRowsInAllTable, dataFromCSV.getList().size());

								CustomPaintListener paintListen = new CustomPaintListener(
										length + numberOfRowsOnScreen);
								natTable.addPaintListener(paintListen);
								natTable.redraw();
								rowCount += onehundredRowsHeight;
								groupCount++;
							}
						} else {
							isEndOfFileReached = false;
						}
						groupCount--;
					}
				} else {
					rowCount = natTable.getVerticalBar().getSelection();

					if (rowCount - onehundredRowsHeight * groupCount >= onehundredRowsHeight) {
						if (!isStartOfFilReached) {

							List<List<String>> newList = csvReader.readNext();

							if (newList.isEmpty()) {
								isEndOfFileReached = true;
							} else if (newList.size() != lengthOfNextNewList) {
								lengthOfNextNewList = newList.size();
							}

							dataFromCSV.addAllElement(newList);
							bodyDataProvider.getList().addAll(newList);

							if (bodyDataProvider.getList().size() > numberOfRowsInAllTable) {
								removeElementFromTo(bodyDataProvider.getList(), 0,
										bodyDataProvider.getList().size() - numberOfRowsInAllTable);
								dataFromCSV.removeFromTo(0, dataFromCSV.getList().size() - numberOfRowsInAllTable);
								CustomPaintListener paintListen = new CustomPaintListener(
										bodyDataProvider.getList().size() / 2 + numberOfRowsInOnePortion
												- lengthOfNextNewList);
								natTable.addPaintListener(paintListen);
								natTable.redraw();
								rowCount -= onehundredRowsHeight;

								groupCount--;
							}
						} else {
							if (++startFileGroupCount == 2) {
								isStartOfFilReached = false;
								startFileGroupCount = 0;
							}
						}
						groupCount++;
					}
				}
			} catch (Exception e2) {
				logger.log(Level.INFO, e2.getMessage());
			}

		}

		private void removeElementFromTo(List<List<String>> list, int fromIndex, int toIndex) {
			for (int i = fromIndex; i < toIndex; i++) {
				list.remove(fromIndex);
			}
		}
	}
}