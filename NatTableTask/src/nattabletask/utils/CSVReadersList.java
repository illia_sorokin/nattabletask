package nattabletask.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


/**
 * @author Illia
 * @version 1.0
 * 
 * Class that contains a list of unique CSVReaders.
 */
public class CSVReadersList {

	private static List<CSVReader> csvReaders = new ArrayList<>();
	
	private CSVReadersList() {}

	/**
	 * Finding the CSVReader by file path.
	 * @param file path.
	 * @return the reader with the specified file name, otherwise the new reader.
	 */
	public static CSVReader getCsvReader(String path) throws FileNotFoundException {
		for (CSVReader csvReader : csvReaders) {
			if (csvReader.getPath().equals(path)) {
				return csvReader;
			}
		}
		CSVReader newReader = new CSVReadersList().new CSVReader(path);
		csvReaders.add(newReader);
		return newReader;
	}

	/**
	 * Removing the CSVReader by file path.
	 * @param file path.
	 */
	public static void removeReader(String path) {
		for (CSVReader csvReader : csvReaders) {
			if (csvReader.getPath().equals(path)) {
				csvReaders.remove(csvReader);
				return;
			}
		}

	}

	
	/**
	 * Class for reading data from a csv file.
	 */
	public class CSVReader {
		private long lastSymbolPosition;
		private List<Long> rowBeginPointerList;
		private RandomAccessFile randomAccessFile;
		private String path;
		private int numberOfRow;

		private CSVReader(String path) throws FileNotFoundException {
			this.lastSymbolPosition = 0;
			this.rowBeginPointerList = new ArrayList<>();
			this.randomAccessFile = new RandomAccessFile(path, "rw");
			this.path = path;
			this.numberOfRow = 50;
		}

		/**
		 * Reads the previous n or less (n = numberOfRow) rows of the file.
		 * @throws IOException if an invalid pointer is passed.
		 * @return n or less (n = numberOfRow) rows of the file, or an empty list if the beginning of the file was reached.
		 */		
		public List<List<String>> readPrevious() throws IOException {
			int count = 0;
			String row;
			List<List<String>> list = new ArrayList<>();

			if (rowBeginPointerList.size() < 6) {
				return list;
			}

			rowBeginPointerList.remove(rowBeginPointerList.size() - 1);
			lastSymbolPosition = rowBeginPointerList.get(rowBeginPointerList.size() - 5);

			randomAccessFile.seek(lastSymbolPosition);
			while (((row = randomAccessFile.readLine()) != null) && count < numberOfRow) {
				count++;
				list.add(Arrays.asList(row.split(",")));
			}
			lastSymbolPosition = randomAccessFile.getFilePointer();

			return list;
		}

		/**
		 * Reads the next n or less (n = numberOfRow) rows of the file. The readHeader method must be called first.
		 * @throws IOException if an invalid pointer is passed.
		 * @return n or less(n = numberOfRow) rows of the file, or an empty list if the end of the file was reached.
		 */
		public List<List<String>> readNext() throws IOException {
			int count = 0;

			String row;
			List<List<String>> list = new ArrayList<>();

			randomAccessFile.seek(rowBeginPointerList.get(rowBeginPointerList.size() - 1));

			while (((row = randomAccessFile.readLine()) != null) && count < numberOfRow) {
				count++;
				list.add(Arrays.asList(row.split(",")));
				lastSymbolPosition = randomAccessFile.getFilePointer();
			}
			if (!rowBeginPointerList.contains(lastSymbolPosition) && lastSymbolPosition != 0
					&& rowBeginPointerList.get(rowBeginPointerList.size() - 1) < lastSymbolPosition) {
				rowBeginPointerList.add(lastSymbolPosition);
			}

			return list;
		}
		

		/**
		 * Reads the header of the file.
		 * @throws IOException if an invalid pointer is passed.
		 * @return header of the file.
		 */
		public List<String> readHeader() throws IOException {
			String row;
			List<String> list = new ArrayList<>();
			randomAccessFile.seek(0);
			if ((row = randomAccessFile.readLine()) != null) {
				list.addAll(Arrays.asList(row.split(",")));
				lastSymbolPosition = randomAccessFile.getFilePointer();
				if (!rowBeginPointerList.contains(lastSymbolPosition)) {
					rowBeginPointerList.add(lastSymbolPosition);
				}
			}
			return list;
		}

		public int getNumberOfRow() {
			return numberOfRow;
		}

		public void setNumberOfRow(int numberOfRow) {
			this.numberOfRow = numberOfRow;
		}

		@Override
		public int hashCode() {
			return Objects.hash(path);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (!(obj instanceof CSVReader)) {
				return false;
			}
			CSVReader other = (CSVReader) obj;
			return getPath().equals(other.getPath());
		}

		public String getPath() {
			return path;
		}
	}

}
